using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Globalization;
using System.Drawing;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace Picker
{
    enum MatchType
    {
        NoMatch,
        ExactMatch,
        ClosestMatch
    };
    
    [Activity (Label = "Picker", MainLauncher = true, Icon="@drawable/Icon")]
    public class PickerActivity : Activity
    {
        /// <summary>
        /// The _color properties.
        /// </summary>
        private static readonly IEnumerable<PropertyInfo> _colorProperties = 
            typeof(Color)
                .GetProperties(BindingFlags.Public | BindingFlags.Static)
                .Where(p => p.PropertyType == typeof (Color));
        
        
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            
            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);
            Random random = new Random();

            SeekBar RedSeekBar = FindViewById<SeekBar> (Resource.Id.redSeekBar);
            SeekBar GreenSeekBar = FindViewById<SeekBar> (Resource.Id.greenSeekBar);
            SeekBar BlueSeekBar = FindViewById<SeekBar> (Resource.Id.blueSeekBar);

            RedSeekBar.ProgressChanged += delegate(object sender, SeekBar.ProgressChangedEventArgs e) {
                setColor();
           };
            GreenSeekBar.ProgressChanged += delegate(object sender, SeekBar.ProgressChangedEventArgs e) {
                setColor();
            };
            BlueSeekBar.ProgressChanged += delegate(object sender, SeekBar.ProgressChangedEventArgs e) {
                setColor();
            };
            RedSeekBar.Progress = random.Next(256);
            GreenSeekBar.Progress = random.Next(256);
            BlueSeekBar.Progress = random.Next(256);
            setColor();
        }


        /// <summary>
        /// Sets the color in ImageView and color name in TextView.
        /// </summary>
        protected void setColor()
        {
            SeekBar RedSeekBar = FindViewById<SeekBar> (Resource.Id.redSeekBar);
            SeekBar GreenSeekBar = FindViewById<SeekBar> (Resource.Id.greenSeekBar);
            SeekBar BlueSeekBar = FindViewById<SeekBar> (Resource.Id.blueSeekBar);
            TextView RedTextView = FindViewById<TextView> (Resource.Id.redValueTextView);
            TextView GreenTextView = FindViewById<TextView> (Resource.Id.greenValueTextView);
            TextView BlueTextView = FindViewById<TextView> (Resource.Id.BlueValueTextView);
            TextView ColorNameTextView = FindViewById<TextView> (Resource.Id.ColorNameTextView);

            int RedBrightness = RedSeekBar.Progress;
            int BlueBrightness = BlueSeekBar.Progress;
            int GreenBrightness = GreenSeekBar.Progress;
            RedTextView.Text = string.Format("Red: {0}( {1} )", RedBrightness, Convert.ToString(RedBrightness, 16));
            GreenTextView.Text = string.Format("Green: {0}( {1} )", GreenBrightness, Convert.ToString(GreenBrightness, 16));
            BlueTextView.Text = string.Format("Blue: {0}( {1} )", BlueBrightness, Convert.ToString(BlueBrightness, 16));
            setImageViewColor( new Android.Graphics.Color(RedBrightness, GreenBrightness, BlueBrightness));
            string colorName = GetApproximateColorName(new Android.Graphics.Color(RedBrightness, GreenBrightness, BlueBrightness));
            ColorNameTextView.Text = colorName=="Transparent"?"White":colorName;
        }

        /// <summary>
        /// Sets the color of the image view.
        /// </summary>
        /// <param name="color">Color.</param>
        private void setImageViewColor(Android.Graphics.Color color)
        {
            ImageView colorImageView = FindViewById<ImageView> (Resource.Id.colorSampleImageView);
            var filledColorBitmap = Android.Graphics.Bitmap.CreateBitmap(160, 160, Android.Graphics.Bitmap.Config.Argb4444);
            filledColorBitmap.EraseColor(color);
            colorImageView.SetImageBitmap(filledColorBitmap);
        }
        
        /// <summary>
        /// Gets the approximate name of the  color.
        /// </summary>
        /// <returns>The approximate color name.</returns>
        /// <param name="color">Color.</param>
        static string GetApproximateColorName(Android.Graphics.Color color)
        {
            int minDistance = int.MaxValue;
            string minColor = Color.Black.Name;

                     
            foreach (var colorProperty in _colorProperties)
            {
                var colorPropertyValue = (Color)colorProperty.GetValue(null, null);
                if (colorPropertyValue.R == color.R
                    && colorPropertyValue.G == color.G
                    && colorPropertyValue.B == color.B)
                {
                    return colorPropertyValue.Name;
                }
                
                int distance = (int)Math.Sqrt( Math.Pow(Math.Abs(colorPropertyValue.R - color.R),2) +
                        Math.Pow(Math.Abs(colorPropertyValue.G - color.G),2) +
                        Math.Pow(Math.Abs(colorPropertyValue.B - color.B),2));
                
                if (distance < minDistance)
                {
                    minDistance = distance;
                    minColor = colorPropertyValue.Name;
                }
            }
            return minColor;
        }
    }
}


